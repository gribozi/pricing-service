package com.agileengine.pricingservice.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.agileengine.pricingservice.model.entity.Pricing;

/**
 * @author Nikolay Koretskyy
 *
 */
public interface PricingRepository extends CrudRepository<Pricing, Integer> {

	List<Pricing> findByName(String name);
	
	List<Pricing> findByModified(Date modified);
	
	@Query("select p from Pricing p where p.modified <= ?1")
	List<Pricing> findByModifiedBefore(Date modified);

}
