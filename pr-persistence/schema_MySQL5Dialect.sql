drop table if exists PRICING;
create table PRICING (ID integer not null auto_increment, MODIFIED_AT datetime, NAME varchar(255), PRICE decimal(7,2), primary key (ID));
