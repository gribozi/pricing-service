package com.agileengine.pricingservice.service.tests;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.agileengine.pricingservice.model.entity.Pricing;
import com.agileengine.pricingservice.model.reports.ModifiedAndPrice;
import com.agileengine.pricingservice.model.reports.NameAndPrice;
import com.agileengine.pricingservice.repository.PricingRepository;
import com.agileengine.pricingservice.service.PricingReportServiceImpl;

/**
 * @author Nikolay Koretskyy
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class PricingReportServiceImplTest {

	private PricingReportServiceImpl pricingReportServiceImpl;
	private PricingRepository mockPricingRepository;

	private List<Pricing> testPricingsMap;
	private List<Pricing> testPricingsNap;

	@Before
	public void setupUp() throws ParseException {
		pricingReportServiceImpl = new PricingReportServiceImpl();
		mockPricingRepository = mock(PricingRepository.class);
		pricingReportServiceImpl.setRepository(mockPricingRepository);

		testPricingsMap = new ArrayList<>();
		testPricingsMap.add(createPricing("MacBook Pro", "1000.00", "2014-10-12 11:11:11.111"));
		testPricingsMap.add(createPricing("MacBook Pro", "2000.00", "2015-12-17 05:14:22.291"));
		testPricingsMap.add(createPricing("MacBook Pro", "3000.99", "2016-11-21 09:45:55.315"));

		testPricingsNap = new ArrayList<>();
		testPricingsNap.add(createPricing("iPhone 5s", "500.00", "2016-11-21 09:45:55.315"));
		testPricingsNap.add(createPricing("iPhone 6s", "700.00", "2016-11-21 09:45:55.315"));
	}

	private Pricing createPricing(String name, String price, String modified) throws ParseException {
		Pricing pricing = new Pricing(name, new BigDecimal(price));
		pricing.setModified(stringToDate(modified));
		return pricing;
	}

	private Date stringToDate(String stringDate) throws ParseException {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		return dfm.parse(stringDate);
	}

	@Test
	public void testGetPricingsByNameSuccess() {

		when(mockPricingRepository.findByName("MacBook Pro")).thenReturn(testPricingsMap);

		List<ModifiedAndPrice> modifiedAndPrices = pricingReportServiceImpl.getPricingsByName("MacBook Pro");

		verify(mockPricingRepository).findByName("MacBook Pro");

		assertEquals(3, modifiedAndPrices.size());

		ModifiedAndPrice modifiedAndPrice;

		modifiedAndPrice = modifiedAndPrices.get(0);
		assertEquals("2014-10-12 11:11:11.111", modifiedAndPrice.getModified());
		assertEquals(new BigDecimal("1000.00"), modifiedAndPrice.getPrice());

		modifiedAndPrice = modifiedAndPrices.get(1);
		assertEquals("2015-12-17 05:14:22.291", modifiedAndPrice.getModified());
		assertEquals(new BigDecimal("2000.00"), modifiedAndPrice.getPrice());

		modifiedAndPrice = modifiedAndPrices.get(2);
		assertEquals("2016-11-21 09:45:55.315", modifiedAndPrice.getModified());
		assertEquals(new BigDecimal("3000.99"), modifiedAndPrice.getPrice());
	}

	@Test
	public void testGetPricingsByNameNotSuccess() {

		testPricingsMap.clear();
		when(mockPricingRepository.findByName("MacBook Air")).thenReturn(testPricingsMap);

		List<ModifiedAndPrice> modifiedAndPrices = pricingReportServiceImpl.getPricingsByName("MacBook Air");

		verify(mockPricingRepository).findByName("MacBook Air");

		assertTrue(modifiedAndPrices.isEmpty());
	}

	@Test
	public void testGetPricingsByModifiedSuccess() throws ParseException {

		when(mockPricingRepository.findByModified(stringToDate("2015-12-17 05:14:22.291"))).thenReturn(testPricingsNap);

		List<NameAndPrice> modifiedAndPrices = pricingReportServiceImpl.getPricingsByModified("2015-12-17 05:14:22.291");

		assertEquals(2, modifiedAndPrices.size());

		NameAndPrice nameAndPrice;

		nameAndPrice = modifiedAndPrices.get(0);
		assertEquals("iPhone 5s", nameAndPrice.getName());
		assertEquals(new BigDecimal("500.00"), nameAndPrice.getPrice());

		nameAndPrice = modifiedAndPrices.get(1);
		assertEquals("iPhone 6s", nameAndPrice.getName());
		assertEquals(new BigDecimal("700.00"), nameAndPrice.getPrice());
	}

	@Test
	public void testGetPricingsByModifiedNotSuccess() throws ParseException {

		testPricingsNap.clear();

		when(mockPricingRepository.findByModified(stringToDate("2015-12-17 05:14:22.291"))).thenReturn(testPricingsNap);

		List<NameAndPrice> modifiedAndPrices = pricingReportServiceImpl.getPricingsByModified("2015-12-17 05:14:22.291");

		verify(mockPricingRepository).findByModified(stringToDate("2015-12-17 05:14:22.291"));

		assertTrue(modifiedAndPrices.isEmpty());
	}
}
