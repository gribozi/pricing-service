package com.agileengine.pricingservice.service.tests;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.agileengine.pricingservice.model.entity.Pricing;
import com.agileengine.pricingservice.service.config.TestServiceConfig;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;

/**
 * @author Nikolay Koretskyy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestServiceConfig.class)
@WebAppConfiguration
public class PricingReportControllerImplTest {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private MockMvc mockMvc;
	
	private ObjectMapper jsonMapper = new ObjectMapper();

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private OpenEntityManagerInViewFilter openEntityManagerInViewFilter;

	private final String mainUri = "/api/pricings";
	private final String actualpricesReporUri = "/api/reports/actualprices";

	// Auxiliary method for creation object which return id. Throws AssertionError if something going wrong.
	private int create(Pricing obj) throws Exception {
		String bodyStr = jsonMapper.writeValueAsString(obj);
		System.out.println(bodyStr);

		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(mainUri)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(bodyStr);

		String location = mockMvc.perform(request).andReturn().getResponse().getHeader("Location");

		int id = -1;
		try {
			id = Integer.parseInt(location.substring(location.lastIndexOf('/') + 1));
			System.out.println(id);
		} catch (Exception ex) {
			Assert.fail("Wrong id in Location header");
		}

		return id;
	}

	private Pricing createPricing(String name, String price, String modified) throws ParseException {
		Pricing pricing = new Pricing(name, new BigDecimal(price));
		pricing.setModified(stringToDate(modified));
		return pricing;
	}

	private Date stringToDate(String stringDate) throws ParseException {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		return dfm.parse(stringDate);
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(openEntityManagerInViewFilter, "/*").build();
		mockMvc.perform(delete(mainUri));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void getActualPricesForTimestamp() throws Exception {

		create(createPricing("iPhone 4s", "400.00", "2013-11-23 11:15:55.675"));
		create(createPricing("iPhone 5s", "700.00", "2015-11-23 11:15:55.675"));
		create(createPricing("iPhone 5s", "500.00", "2016-10-22 10:35:55.435"));
		create(createPricing("iPhone 6s", "1700.00", "2015-11-23 11:15:55.675"));
		create(createPricing("iPhone 6s", "1500.00", "2016-10-22 10:35:55.435"));

		mockMvc.perform(get(actualpricesReporUri + "?modified=2017-01-01 00:00:00.001"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$", hasSize(3)))
				.andExpect(jsonPath("$[0].name", is("iPhone 4s")))
				.andExpect(jsonPath("$[0].price", is(400.00)))
				.andExpect(jsonPath("$[1].name", is("iPhone 5s")))
				.andExpect(jsonPath("$[1].price", is(500.00)))
				.andExpect(jsonPath("$[2].name", is("iPhone 6s")))
				.andExpect(jsonPath("$[2].price", is(1500.00)));
		
		mockMvc.perform(get(actualpricesReporUri + "?modified=2015-12-12 11:15:55.675"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$", hasSize(3)))
				.andExpect(jsonPath("$[0].name", is("iPhone 4s")))
				.andExpect(jsonPath("$[0].price", is(400.00)))
				.andExpect(jsonPath("$[1].name", is("iPhone 5s")))
				.andExpect(jsonPath("$[1].price", is(700.00)))
				.andExpect(jsonPath("$[2].name", is("iPhone 6s")))
				.andExpect(jsonPath("$[2].price", is(1700.00)));

		mockMvc.perform(get(actualpricesReporUri + "?modified=2015-11-23 11:15:55.675"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$", hasSize(3)))
				.andExpect(jsonPath("$[0].name", is("iPhone 4s")))
				.andExpect(jsonPath("$[0].price", is(400.00)))
				.andExpect(jsonPath("$[1].name", is("iPhone 5s")))
				.andExpect(jsonPath("$[1].price", is(700.00)))
				.andExpect(jsonPath("$[2].name", is("iPhone 6s")))
				.andExpect(jsonPath("$[2].price", is(1700.00)));

		mockMvc.perform(get(actualpricesReporUri + "?modified=2014-11-23 11:15:55.675"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].name", is("iPhone 4s")))
				.andExpect(jsonPath("$[0].price", is(400.00)));
		
		mockMvc.perform(get(actualpricesReporUri + "?modified=2012-12-12 11:15:55.675"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$", hasSize(0)));

	}
}