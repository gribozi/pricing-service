package com.agileengine.pricingservice.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.agileengine.pricingservice.model.entity.Pricing;

/**
 * @author Nikolay Koretskyy
 *
 */
public interface PricingCrudController {

	public ResponseEntity<Pricing> get(int id);

	public ResponseEntity<List<Pricing>> getAll();

	public ResponseEntity<Void> create(Pricing pricing);

	public ResponseEntity<Pricing> update(int id, Pricing pricing);

	public ResponseEntity<Void> delete(int id);

	public ResponseEntity<Void> deleteAll();

}
