package com.agileengine.pricingservice.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.agileengine.pricingservice.model.reports.ModifiedAndPrice;
import com.agileengine.pricingservice.model.reports.NameAndPrice;

/**
 * @author Nikolay Koretskyy
 *
 */
public interface PricingReportController {

	public ResponseEntity<List<ModifiedAndPrice>> getPricingsByName(String name);

	public ResponseEntity<List<NameAndPrice>> getPricingsByModified(String modified) throws ParseException;

	public ResponseEntity<List<NameAndPrice>> getActualPricesForTimestamp(String modified) throws ParseException;

}
