package com.agileengine.pricingservice.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.agileengine.pricingservice.model.reports.ModifiedAndPrice;
import com.agileengine.pricingservice.model.reports.NameAndPrice;
import com.agileengine.pricingservice.service.PricingReportService;

/**
 * Rest Controller for Pricing reports.
 * 
 * @author Nikolay Koretskyy
 *
 */
@RestController
@RequestMapping(value = "/api/reports")
public class PricingReportControllerImpl implements PricingReportController {

	@Autowired
	private PricingReportService pricingReportService;

	/**
	 * Retrieve modifieds and prices by product name.
	 */
	@Override
	@RequestMapping(value = "/map", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<ModifiedAndPrice>> getPricingsByName(@RequestParam String name) {

		List<ModifiedAndPrice> modifiedAndPrices = pricingReportService.getPricingsByName(name);
		return new ResponseEntity<List<ModifiedAndPrice>>(modifiedAndPrices, HttpStatus.OK);
	}

	/**
	 * Retrieve names and prices by product modified.
	 */
	@Override
	@RequestMapping(value = "/nap", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<NameAndPrice>> getPricingsByModified(@RequestParam String modified) throws ParseException {

		List<NameAndPrice> nameAndPrice = pricingReportService.getPricingsByModified(modified);
		return new ResponseEntity<List<NameAndPrice>>(nameAndPrice, HttpStatus.OK);
	}

	/**
	 * Retrieve actual prices for certain date/time.
	 */
	@Override
	@RequestMapping(value = "/actualprices", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<NameAndPrice>> getActualPricesForTimestamp(@RequestParam String modified) throws ParseException {

		List<NameAndPrice> nameAndPrice = pricingReportService.getActualPricesForTimestamp(modified);
		return new ResponseEntity<List<NameAndPrice>>(nameAndPrice, HttpStatus.OK);
	}
	
}
