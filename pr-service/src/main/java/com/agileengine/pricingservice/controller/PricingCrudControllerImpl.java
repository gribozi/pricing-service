package com.agileengine.pricingservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.agileengine.pricingservice.model.entity.Pricing;
import com.agileengine.pricingservice.service.PricingCrudService;

/**
 * Rest Controller for Pricing entity.
 * 
 * @author Nikolay Koretskyy
 *
 */
@RestController
@RequestMapping(value = "/api/pricings")
public class PricingCrudControllerImpl implements PricingCrudController {

	@Autowired
	private PricingCrudService pricingCrudService;

	/**
	 * Retrieve single pricing.
	 */
	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Pricing> get(@PathVariable("id") int id) {

		Pricing pricing = pricingCrudService.get(id);
		if (pricing == null) {
			return new ResponseEntity<Pricing>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Pricing>(pricing, new HttpHeaders(), HttpStatus.OK);
		}
	}

	/**
	 * Retrieve all pricings.
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Pricing>> getAll() {

		List<Pricing> pricings = (List<Pricing>) pricingCrudService.getAll();
		return new ResponseEntity<List<Pricing>>(pricings, HttpStatus.OK);
	}

	/**
	 * Create pricing.
	 */
	@Override
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Void> create(@Valid @RequestBody Pricing pricing) {

		if ((pricing == null) || (pricing.getId() != 0)) {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}

		Pricing createdPricing = pricingCrudService.create(pricing);
		if (createdPricing == null) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		HttpHeaders headers = new HttpHeaders();
		UriComponentsBuilder ucBuilder = UriComponentsBuilder.newInstance();
		headers.setLocation(ucBuilder.path("/api/pricings/{id}").buildAndExpand(createdPricing.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	/**
	 * Update pricing.
	 */
	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Pricing> update(@PathVariable("id") int id, @Valid @RequestBody Pricing pricing) {

		if ((pricing == null) || (pricing.getId() != id)) {
			return new ResponseEntity<Pricing>(HttpStatus.BAD_REQUEST);
		}

		Pricing updatedPricing = pricingCrudService.update(id, pricing);
		if (updatedPricing == null) {
			return new ResponseEntity<Pricing>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Pricing>(pricing, HttpStatus.OK);
		}
	}

	/**
	 * Delete pricing.
	 */
	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable("id") int id) {

		if (!pricingCrudService.delete(id)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * Delete all pricings.
	 */
	@Override
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteAll() {

		pricingCrudService.deleteAll();
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}
