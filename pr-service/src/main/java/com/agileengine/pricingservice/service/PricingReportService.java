package com.agileengine.pricingservice.service;

import java.text.ParseException;
import java.util.List;

import com.agileengine.pricingservice.model.reports.ModifiedAndPrice;
import com.agileengine.pricingservice.model.reports.NameAndPrice;

/**
 * @author Nikolay Koretskyy
 *
 */
public interface PricingReportService {

	public List<ModifiedAndPrice> getPricingsByName(String name);

	public List<NameAndPrice> getPricingsByModified(String modified) throws ParseException;

	public List<NameAndPrice> getActualPricesForTimestamp(String modified) throws ParseException;

}
