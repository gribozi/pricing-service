package com.agileengine.pricingservice.service;

import java.util.List;

import com.agileengine.pricingservice.model.entity.Pricing;

/**
 * @author Nikolay Koretskyy
 *
 */
public interface PricingCrudService {

	public Pricing get(int id);

	public List<Pricing> getAll();

	public Pricing create(Pricing pricing);

	public Pricing update(int id, Pricing pricing);

	public boolean delete(int id);

	public void deleteAll();

}
