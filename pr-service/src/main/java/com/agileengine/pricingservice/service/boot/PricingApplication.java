package com.agileengine.pricingservice.service.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.agileengine.pricingservice")
@ComponentScan("com.agileengine.pricingservice")
/**
 * Entry point to start Spring Boot Application.
 * 
 * @author Nikolay Koretskyy
 *
 */
public class PricingApplication {

	public static void main(String[] args) {
		SpringApplication.run(PricingApplication.class, args);
	}
}
