package com.agileengine.pricingservice.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableJpaRepositories("com.agileengine.pricingservice")
@ComponentScan("com.agileengine.pricingservice")
@EnableWebMvc
public class TestServiceConfig {

}
