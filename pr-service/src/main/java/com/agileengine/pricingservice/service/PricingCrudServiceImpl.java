package com.agileengine.pricingservice.service;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agileengine.pricingservice.model.entity.Pricing;
import com.agileengine.pricingservice.repository.PricingRepository;

/**
 * @author Nikolay Koretskyy
 *
 */
@Service
public class PricingCrudServiceImpl implements PricingCrudService {

	@Autowired
	private PricingRepository repository;

	private static final Logger log = Logger.getLogger(PricingCrudServiceImpl.class);

	/**
	 * Retrieve single pricing.
	 */
	@Override
	public Pricing get(int id) {

		log.info("Fetching pricing with id " + id);

		Pricing pricing = repository.findOne(id);
		if (pricing == null) {
			log.info("Pricing with id " + id + " was not found");
			return null;
		}
		return pricing;
	}

	/**
	 * Retrieve all pricings.
	 */
	@Override
	public List<Pricing> getAll() {
		log.info("Fetching all pricings");
		return (List<Pricing>) repository.findAll();
	}

	/**
	 * Create pricing.
	 */
	@Override
	public Pricing create(Pricing pricing) {

		log.info("Creating pricing " + pricing.getName());

		if (repository.exists(pricing.getId())) {
			log.info("Pricing with id " + pricing.getId() + " already exist");
			return null;
		}

		if (pricing.getModified() == null) {
			pricing.setModified(new Date());
		}
		Pricing newPricing = repository.save(pricing);
		log.info("Pricing " + newPricing.getName() + " was created");
		return newPricing;
	}

	/**
	 * Update pricing.
	 */
	@Override
	public Pricing update(int id, Pricing pricing) {

		log.info("Updating pricing with id " + id);

		Pricing pricingOld = repository.findOne(id);
		if (pricingOld == null) {
			log.info("Pricing with id " + id + " was not found");
			return null;
		} else {
			pricing.setModified(new Date());
			Pricing updatedPricing = repository.save(pricing);
			log.info("Pricing with id " + id + " was updated");
			return updatedPricing;
		}
	}

	/**
	 * Delete pricing.
	 */
	@Override
	public boolean delete(int id) {

		log.info("Fetching & deleting pricing with id " + id);

		if (!repository.exists(id)) {
			log.info("Unable to delete. Pricing with id " + id + " was not found");
			return false;
		}

		repository.delete(id);
		log.info("Pricing with id " + id + " was deleted");
		return true;
	}

	/**
	 * Delete all pricings.
	 */
	@Override
	public void deleteAll() {
		log.info("Deleting all pricings");
		repository.deleteAll();
		log.info("All pricings was deleted");
	}

}
