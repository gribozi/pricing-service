package com.agileengine.pricingservice.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agileengine.pricingservice.model.entity.Pricing;
import com.agileengine.pricingservice.model.reports.ModifiedAndPrice;
import com.agileengine.pricingservice.model.reports.NameAndPrice;
import com.agileengine.pricingservice.repository.PricingRepository;

/**
 * @author Nikolay Koretskyy
 *
 */
@Service
public class PricingReportServiceImpl implements PricingReportService {

	@Autowired
	private PricingRepository repository;

	private static final Logger log = Logger.getLogger(PricingReportServiceImpl.class);

	public PricingRepository getRepository() {
		return repository;
	}

	public void setRepository(PricingRepository repository) {
		this.repository = repository;
	}

	/**
	 * Retrieve pricings by product name.
	 */
	@Override
	public List<ModifiedAndPrice> getPricingsByName(String name) {

		log.info("Fetching pricings by product name " + name);

		List<Pricing> pricings = repository.findByName(name);
		List<ModifiedAndPrice> modifiedAndPrices = pricings.stream().map(pricing -> {
			Date modified = pricing.getModified();
			Timestamp timestamp = new Timestamp(modified.getTime());
			ModifiedAndPrice modifiedAndPrice = new ModifiedAndPrice(timestamp.toString(), pricing.getPrice());
			return modifiedAndPrice;
		}).collect(Collectors.toList());

		return modifiedAndPrices;
	}

	/**
	 * Retrieve pricings by product modified date/time.
	 */
	@Override
	public List<NameAndPrice> getPricingsByModified(String modified) throws ParseException {

		log.info("Fetching pricings by product modified date/time " + modified);

		List<Pricing> pricings = repository.findByModified(stringToDate(modified));
		List<NameAndPrice> nameAndPrices = pricings.stream()
				.map(pricing -> new NameAndPrice(pricing.getName(), pricing.getPrice())).collect(Collectors.toList());

		return nameAndPrices;
	}

	/**
	 * Retrieve actual prices for certain date/time.
	 */
	@Override
	public List<NameAndPrice> getActualPricesForTimestamp(String certainTimestamp) throws ParseException {

		log.info("Fetching actual prices for certain date/time " + certainTimestamp);

		List<Pricing> pricings = repository.findByModifiedBefore(stringToDate(certainTimestamp));

		List<NameAndPrice> nameAndPrices = pricings.stream().filter((pricing) -> {
			Set<Date> modifiedsForCurentName = new HashSet<>();
			pricings.forEach((p) -> {
				if (p.getName().equals(pricing.getName())) {
					modifiedsForCurentName.add(p.getModified());
				}
			});
			Date maxDateForCurentName = modifiedsForCurentName.stream().max(Date::compareTo).get();

			if (pricing.getModified().equals(maxDateForCurentName)) {
				return true;
			}
			return false;

		}).map(pricing -> new NameAndPrice(pricing.getName(), pricing.getPrice())).collect(Collectors.toList());

		return nameAndPrices;
	}

	private Date stringToDate(String stringDate) throws ParseException {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		return dfm.parse(stringDate);
	}
}
