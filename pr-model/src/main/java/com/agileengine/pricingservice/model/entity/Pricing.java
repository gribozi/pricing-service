package com.agileengine.pricingservice.model.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * The type of a pricing object.
 * 
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Table(name = "PRICING")
public class Pricing extends IdentityEntity {

	private String name;
	private BigDecimal price;

	public Pricing() {
		super();
	}

	public Pricing(String name, BigDecimal price) {
		super();
		this.name = name;
		this.price = price;
	}

	public Pricing(int id, String name) {
		setId(id);
		setName(name);
	}

	public Pricing(String name) {
		this.setName(name);
	}

	@NotNull
	@Size(max = 80)
	@Column(name = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@NotNull
	@Digits(integer = 5, fraction = 2)
	@Column(name = "PRICE", precision = 7, scale = 2)
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
