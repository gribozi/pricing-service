package com.agileengine.pricingservice.model.reports;

import java.math.BigDecimal;

/**
 * @author Nikolay Koretskyy
 *
 */
public class NameAndPrice {

	private String name;
	private BigDecimal price;

	public NameAndPrice(String name, BigDecimal price) {
		super();
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
