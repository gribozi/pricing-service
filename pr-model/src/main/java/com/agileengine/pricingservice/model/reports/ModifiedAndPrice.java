package com.agileengine.pricingservice.model.reports;

import java.math.BigDecimal;

/**
 * @author Nikolay Koretskyy
 *
 */
public class ModifiedAndPrice {

	private String modified;
	private BigDecimal price;

	public ModifiedAndPrice(String modified, BigDecimal price) {
		super();
		this.modified = modified;
		this.price = price;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
